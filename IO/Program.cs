﻿using System;
using System.Diagnostics;


namespace IO
{
    class Program
    {
        static void Main(string[] args)
        {
            Process python = new Process();
            python.StartInfo.FileName = @"C:\Python27\python.exe";
            python.StartInfo.UseShellExecute = false;
            python.StartInfo.RedirectStandardInput = true;
            python.StartInfo.RedirectStandardOutput = true;
            python.Start();
            var commands = "print 'Hello World'\n";
            Console.WriteLine("Sent to Python:");
            Console.WriteLine(commands);
            python.StandardInput.Write(commands);
            python.StandardInput.Flush();
            python.StandardInput.Close();
            Console.WriteLine("Received from Python:");
            Console.WriteLine(python.StandardOutput.ReadToEnd());
            python.WaitForExit();
            Console.WriteLine("Python exited with code " + python.ExitCode);

        }
    }
}
