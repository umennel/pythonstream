﻿using System;
using System.Diagnostics;
using System.Threading;


namespace IOAsync
{
    class Program
    {
        static void Main(string[] args)
        {
            Process python = new Process();
            python.StartInfo.FileName = @"C:\Python27\python.exe";
            python.StartInfo.UseShellExecute = false;
            python.StartInfo.RedirectStandardInput = true;
            python.StartInfo.RedirectStandardOutput = true;
            python.OutputDataReceived += (sender, eventArgs) =>
            {
                //Ignore EOF
                if (eventArgs.Data != null)
                {
                    Console.WriteLine("Received from Python:");
                    Console.WriteLine(eventArgs.Data);
                }
            };
            python.Start();
            var commands = "print 'Hello World'\n";
            Console.WriteLine("Sent to Python:");
            Console.WriteLine(commands);
            python.StandardInput.Write(commands);
            python.StandardInput.Flush();
            python.StandardInput.Close();
            python.BeginOutputReadLine();
            python.WaitForExit();
            Console.WriteLine("Python exited with code " + python.ExitCode);
            // Give the application time to finish
            Thread.Sleep(1000);
        }
    }
}
