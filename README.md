# Simple Python via standard stream examples #

C# examples of invoking a 'Hello World' script by running Python as external process and sending data through standard input and getting the result by reading back standard output.
Note that you have to close standard input to invoke the script. This means that you can execute only one script per process (i.e. interactive mode is not possible).
The output has to be parsed to get some information out of it (this might get difficult in case of error messages/unstructured data).
To check for error, it is much simpler to check the exit code: 0 for success, 1 for error.
There is also an asynchronous version which is useful for GUIs or event driven applications.